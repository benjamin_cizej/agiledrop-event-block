<?php

namespace Drupal\Tests\agiledropeventblock\Unit;

use Drupal\agiledropeventblock\Agiledrop\DateDifferenceCalculator;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test for date difference calculator
 *
 * @group agiledropeventblock
 */
class DateDifferenceCalculatorTest extends UnitTestCase
{
    /** @var DateDifferenceCalculator */
    protected $instance;

    public function setUp()
    {
        $this->instance = new DateDifferenceCalculator();
    }

    /**
     * @covers Drupal\agiledropeventblock\Agiledrop\DateDifferenceCalculator::daysDifference
     * @dataProvider datesProvider
     */
    public function testDaysDifference($start, $end, $expected)
    {
        $this->assertEquals($expected, $this->instance->daysDifference($start, $end));
    }

    public function datesProvider()
    {
        return [
            [
                new \DateTime('2019-01-01'),
                new \DateTime('2019-01-01'),
                0
            ],
            [
                new \DateTime('2019-01-01'),
                new \DateTime('2019-01-02'),
                1
            ],
            [
                new \DateTime('2019-01-02'),
                new \DateTime('2019-01-01'),
                -1
            ],
            [
                new \DateTime('2019-01-01 23:59:59'),
                new \DateTime('2019-01-02 00:01:00'),
                1
            ],
            [
                new \DateTime('2019-01-01 23:59:59'),
                new \DateTime('2019-01-03 00:00:00'),
                2
            ],
            [
                new \DateTime('2019-01-02 13:43:44'),
                new \DateTime('2019-01-01 14:23:00'),
                -1
            ]
        ];
    }

    public function tearDown()
    {
        unset($this->instance);
    }
}