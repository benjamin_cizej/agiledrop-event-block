<?php

namespace Drupal\agiledropeventblock\Agiledrop;

use \DateTime;

class DateDifferenceCalculator
{
    public function daysDifferenceFromNow(DateTime $dateTime)
    {
        return $this->daysDifference(new DateTime(), $dateTime);
    }

    public function daysDifference(DateTime $start, DateTime $end)
    {
        return (int)$start->setTime(0, 0, 0)->diff($end->setTime(0, 0, 0))->format("%r%a");
    }
}