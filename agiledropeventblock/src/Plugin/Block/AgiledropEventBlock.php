<?php

namespace Drupal\agiledropeventblock\Plugin\Block;

use DateTime;
use Drupal\agiledropeventblock\Agiledrop\DateDifferenceCalculator;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 * id = "block_agiledropeventblock",
 * admin_label = @Translation("Agiledrop Event Block"),
 * )
 */
class AgiledropEventBlock extends BlockBase implements ContainerFactoryPluginInterface
{
    /** @var DateDifferenceCalculator */
    protected $calculator;

    public function __construct(array $configuration, $plugin_id, $plugin_definition, $calculator)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->calculator = $calculator;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        /** @var NodeInterface $node */
        $node = \Drupal::routeMatch()->getParameter('node');
        if (!$node) {
            return [];
        }
        if ($node->getType() != 'event') {
            return [];
        }

        $date = new DateTime($node->field_event_date->value);
        if (!$date) {
            return [];
        }

        return [
            '#type' => 'markup',
            '#markup' => $this->getBlockText($date)
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheMaxAge()
    {
        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('agiledropeventblock.calculator')
        );
    }

    protected function getBlockText(DateTime $eventDate)
    {
        $difference = $this->calculator->daysDifferenceFromNow($eventDate);
        if ($difference > 0) {
            return sprintf('%s day(s) left until event starts.', $difference);
        }
        if ($difference < 0) {
            return 'This event already passed.';
        }

        return 'This event is happening today.';
    }
}